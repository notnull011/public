#!/usr/bin/env python
# -*- coding: utf-8 -*-
import argparse
import os
from sys import exit
from subprocess import Popen, PIPE
from config import config


def error(mesg):
    print "\nERROR: %s\n" % mesg
    exit(1)

def args_parse():
    a = argparse.ArgumentParser()

    a.add_argument('-n','--new',help="New install", action='store_true')
    a.add_argument('--sync-home',help="Sync Home", action='store_true')
    a.add_argument('--sync-path',help="Sync Path", action='store_true')

    return a.parse_args()


def install():

    print "New install\n"

    print "Updating packages repo: apt update\n"

    aptUpdate = Popen("apt update", stdout=PIPE, stderr=PIPE, shell=True)
    aptUpdateStatus = aptUpdate.wait()

    # Exit if there is any error
    if aptUpdateStatus != 0 : error("There was a problem running: apt update")
 
    print "The following packages will be installed via apt"

    aptPackages = config['apt']

    for i in aptPackages:
        print "\t- %s" % i

    print "\nInstalling packages"
    aptInstall = Popen("apt install -y %s " % " ".join( aptPackages) , stdout=PIPE, stderr=PIPE, shell=True)
    aptInstallStatus =  aptInstall.wait()

    
def main():
    
    rootUid = 0

    # Exit if user is not root
    if os.getuid() != 0: error("You must run as root!")

    args = args_parse()

    syncHome = args.sync_home
    syncPath = args.sync_path

    newInstall = args.new

    if newInstall:
        install()

if __name__ == "__main__":
    main()
