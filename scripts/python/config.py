config =  {}

# Resotre Destination
config['dest'] = '/'

# Source 
config['src'] = '/media/notnull'

# Apt Packages to install 
config['apt'] = [
    'terminator',
    'subversion',
    'wmctrl',
    'xdotool',
    'glances',
    'htop',
    'krdc',
    'libyaml-perl',
    'libdatetime-perl',
    'libjson-perl',
    'git',
    'zsh',
    'expect',
    'wget',
    'lsof',
    'opensc',
    'xbindkeys',
    'partitionmanager',
    'yakuake',
    'albert',
    'plank',
    'focuswriter',
    'corebird',
    'wireshark',
    'redshift',
    'redshift-gtk',
    'openssh-server',
    'jq',
    'python-mysqldb',
    'libnet-openssh-perl',
    'libnet-dns-perl'
]

# Pip Packages
config['pip'] = {
    'url': 'https://bootstrap.pypa.io/get-pip.py',
    'pkgs': [
        'flask',
        'locust'
    ]
}
